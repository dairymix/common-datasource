/**
 * 
 */
package com.common.distributed.datasource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.common.distributed.datasource.core.shard.DataBaseShard;
import com.common.distributed.datasource.core.shard.TableBeanShard;
import com.common.distributed.datasource.core.shard.support.TableBean;
import com.common.distributed.datasource.route.support.TableNameBean;
import com.common.distributed.datasource.spring.config.schema.CommonDatasourceSchema;
import com.common.distributed.datasource.spring.config.schema.DataBaseSchema;
import com.common.distributed.datasource.spring.config.schema.TableSchema;
/**
 * @author liubing1 
 * 分布式数据源核心类,单例模式
 */
public class CommonDatasource {

	private CommonDatasource() {
		
	}

	private static class SingletonHolder {
		static final CommonDatasource instance = new CommonDatasource();
	}

	public static CommonDatasource getInstance() {
		return SingletonHolder.instance;
	}
	/**
	 * 根据表的字段名
	 * 获取分库分表选择的数据库
	 * @param distributedClientSchema
	 * @param tablenamefield
	 * @return
	 */
	public DataBaseSchema getShardDataSoure(
			CommonDatasourceSchema commonDatasourceSchema,
			TableNameBean tableNameBean) {
		// TODO Auto-generated method stub
		Map<String, DataBaseSchema> map=commonDatasourceSchema.getMap();
		List<DataBaseSchema> dataBaseSchemas=new ArrayList<DataBaseSchema>();
		String tablename="";
		Boolean flag=false;
		for(String key:map.keySet()){//找出跳出循环
			List<TableSchema> tableSchemas=map.get(key).getTableSchemas();
			for(TableSchema tableSchema:tableSchemas){
				if(tableSchema.getTablefield().equals(tableNameBean.getTablefield())){
					tablename=tableSchema.getTablename();
					flag=true;
					break;
				}
			}
			if(flag){
				break;
			}
		}
		Map<String, Set<String>> tablenameMapingDatasources=commonDatasourceSchema.collectTablenameMapingDatasource();
		Set<String> databases=tablenameMapingDatasources.get(tablename);
		for(String database:databases){
			dataBaseSchemas.add(commonDatasourceSchema.getMap().get(database));
		}
		DataBaseShard baseSchemaHashShard=new DataBaseShard(dataBaseSchemas);
		return baseSchemaHashShard.getShardInfo(tableNameBean.getTablefieldvalue());
	}
	/**
	 * 获取分库分表选择的表名
	 * @param distributedClientSchema
	 * @param tablenamefield
	 * @param databsename
	 * @return
	 */
	public TableBean getShardTableName(
			CommonDatasourceSchema commonDatasourceSchema,
			TableNameBean tableNameBean, String databsename) {
		// TODO Auto-generated method stub
		Map<String, List<TableSchema>> maps=commonDatasourceSchema.collectDataBaseMappingTablename();
		List<TableSchema> tableSchemas=maps.get(databsename);
		TableSchema result=new TableSchema();
		for(TableSchema tableSchema:tableSchemas){
			if(tableSchema.getTablefield().equals(tableNameBean.getTablefield())){
				result=tableSchema;
				break;
			}
		}
		
		List<TableBean> tableBeans=new ArrayList<TableBean>();
		for(int i=0;i<result.getCount();i++){
			TableBean tableBean=new TableBean();
			tableBean.setName(result.getTablename());
			tableBean.setPrefixname(result.getPrefixname()+i);
			tableBeans.add(tableBean);
		}
		TableBeanShard beanHashShard=new TableBeanShard(tableBeans);
		return beanHashShard.getShardInfo(tableNameBean.getTablefieldvalue());
	}
}
