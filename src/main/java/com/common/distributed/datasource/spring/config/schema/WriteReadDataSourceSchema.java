/**
 * 
 */
package com.common.distributed.datasource.spring.config.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.common.distributed.datasource.readwritestragy.CommonDatasourceServer;

/**
 * @author liubing1
 * 读写混合数据源
 */
public class WriteReadDataSourceSchema implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4892479165465186610L;
	
	private List<CommonDatasourceSchema> commonDatasourceSchemas =new ArrayList<CommonDatasourceSchema>();
	
	public WriteReadDataSourceSchema(){
		
	}

	/**
	 * @return the commonDatasourceSchemas
	 */
	public List<CommonDatasourceSchema> getCommonDatasourceSchemas() {
		return commonDatasourceSchemas;
	}

	/**
	 * @param commonDatasourceSchemas the commonDatasourceSchemas to set
	 */
	public void setCommonDatasourceSchemas(
			List<CommonDatasourceSchema> commonDatasourceSchemas) {
		this.commonDatasourceSchemas = commonDatasourceSchemas;
	}
	
	public Map<String,List<CommonDatasourceServer>> groupDataSourceByType(){
		Map<String,List<CommonDatasourceServer>> maps=new HashMap<String, List<CommonDatasourceServer>>();
		
		for(CommonDatasourceSchema commonDatasourceSchema:commonDatasourceSchemas){
			if(maps.containsKey(commonDatasourceSchema.getType())){
				List<CommonDatasourceServer> datasourceSchemas=maps.get(commonDatasourceSchema.getType());
				CommonDatasourceServer commonDatasourceServer=new CommonDatasourceServer();
				commonDatasourceServer.setCommonDatasourceSchema(commonDatasourceSchema);
				commonDatasourceServer.setWeight(Integer.parseInt(commonDatasourceSchema.getWeight()));
				commonDatasourceServer.setEffectiveWeight(Integer.parseInt(commonDatasourceSchema.getWeight()));
				commonDatasourceServer.setCurrentWeight(0);
				datasourceSchemas.add(commonDatasourceServer);
			}else{
				List<CommonDatasourceServer> datasourceSchemas =new ArrayList<CommonDatasourceServer>();
				CommonDatasourceServer commonDatasourceServer=new CommonDatasourceServer();
				commonDatasourceServer.setCommonDatasourceSchema(commonDatasourceSchema);
				commonDatasourceServer.setWeight(Integer.parseInt(commonDatasourceSchema.getWeight()));
				commonDatasourceServer.setEffectiveWeight(Integer.parseInt(commonDatasourceSchema.getWeight()));
				commonDatasourceServer.setCurrentWeight(0);
				datasourceSchemas.add(commonDatasourceServer);
				maps.put(commonDatasourceSchema.getType(), datasourceSchemas);
			}
		}
		return maps;
		
	}
}
