/**
 * 
 */
package com.common.distributed.datasource.spring.config.schema;

import java.io.Serializable;
import java.util.List;

/**
 * @author liubing1
 *  数据源
 */
public class DataBaseSchema implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1023587279430654493L;

	private List<TableSchema> tableSchemas;// 对应的表

	private String databaseName;

	private String datasourceId;
	
	public DataBaseSchema(){
		
	}
	
	public DataBaseSchema(String databaseName, String datasourceId,
			List<TableSchema> tableSchemas) {
		super();
		
		this.tableSchemas = tableSchemas;
	}

	/**
	 * @return the tableSchemas
	 */
	public List<TableSchema> getTableSchemas() {
		return tableSchemas;
	}

	/**
	 * @param tableSchemas the tableSchemas to set
	 */
	public void setTableSchemas(List<TableSchema> tableSchemas) {
		this.tableSchemas = tableSchemas;
	}

	/**
	 * @return the databaseName
	 */
	public String getDatabaseName() {
		return databaseName;
	}

	/**
	 * @param databaseName the databaseName to set
	 */
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	/**
	 * @return the datasourceId
	 */
	public String getDatasourceId() {
		return datasourceId;
	}

	/**
	 * @param datasourceId the datasourceId to set
	 */
	public void setDatasourceId(String datasourceId) {
		this.datasourceId = datasourceId;
	}
}
