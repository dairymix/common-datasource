package test.common.distributed.datasource.mybatis;

import com.common.distributed.datasource.route.CommonDatasourceRoute;
import com.common.distributed.datasource.spring.config.schema.CommonDatasourceSchema;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yif on 3/29/15.
 */

@ContextConfiguration(locations = { "classpath:mybatis/spring.xml" })
public class MybatisTest {

	public void setUp() {

	}

	public void tearDown() {

	}

	@Inject
	private DemoDao demoDao;
	@Inject
	private CommonDatasourceSchema commonDatasourceSchema;

	public void testInsertDemo() {
		DemoParam demo = new DemoParam();
		demo.setId("barcelona10");
		demo.setName("messi");
		demo.setNumber("10");
		demo = (DemoParam) CommonDatasourceRoute.getInstance()
				.doDataSourceRoute(commonDatasourceSchema, demo);
		demoDao.insertDemo(demo);
	}

	public void testFindDemo() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", "barcelona10");
		params = (Map<String, Object>) CommonDatasourceRoute.getInstance()
				.doDataSourceRoute(commonDatasourceSchema, params);
		System.out.println(params.get("tablename"));
		Demo demo = demoDao.findDemo(params);
		System.out.println(demo.getId() + "\t" + demo.getName() + "\t"
				+ demo.getNumber());
	}
}