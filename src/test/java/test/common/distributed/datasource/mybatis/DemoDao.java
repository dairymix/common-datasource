package test.common.distributed.datasource.mybatis;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import test.common.distributed.datasource.mybatis.DemoParam;

import javax.inject.Inject;
import java.util.Map;

/**
 * Created by yif on 3/29/15.
 */
@Repository
public class DemoDao {
    @Inject
    private SqlSession sqlSession;

    public void insertDemo(DemoParam demo) {
        sqlSession.insert("test.common.distributed.datasource.mybatis.DemoDao.insertDemo", demo);
    }

    public Demo findDemo(Map<String, Object> params) {
        return sqlSession.selectOne("test.common.distributed.datasource.mybatis.DemoDao.findDemo", params);
    }
}
