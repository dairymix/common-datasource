package test.common.distributed.datasource.mybatis;

/**
 * Created by yif on 3/29/15.
 */
public class Demo {

    private String id;

    private String name;

    private String number;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
