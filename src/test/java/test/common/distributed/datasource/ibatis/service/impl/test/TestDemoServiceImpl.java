/**
 * 
 */
package test.common.distributed.datasource.ibatis.service.impl.test;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import test.common.distributed.datasource.ibatis.model.DemoModel;
import test.common.distributed.datasource.ibatis.model.DemoParam;
import test.common.distributed.datasource.ibatis.service.IDemoService;

import com.common.distributed.datasource.route.CommonDatasourceRoute;
import com.common.distributed.datasource.spring.config.schema.CommonDatasourceSchema;

import junit.framework.TestCase;

/**
 * @author liubing1
 *
 */
public class TestDemoServiceImpl extends TestCase {
	
	@SuppressWarnings("unchecked")
	public void testFind() throws Exception{
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		CommonDatasourceSchema clientSchema=(CommonDatasourceSchema) context.getBean("democlient");
		IDemoService demoService=(IDemoService) context.getBean("demoService");
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("id", 12);
		params= (Map<String, Object>) CommonDatasourceRoute.getInstance().doDataSourceRoute(clientSchema, params);
		DemoModel demoModel=demoService.findById(params);
		System.out.println("name:"+demoModel.getName()+",id:"+demoModel.getId()+",number:"+demoModel.getNumber());
	}
	/**
	 * 测试删除
	 * @throws Exception
	 */
	public void testDelete() throws Exception{
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		CommonDatasourceSchema clientSchema=(CommonDatasourceSchema) context.getBean("democlient");
		IDemoService demoService=(IDemoService) context.getBean("demoService");
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("id", 12);
		params= (Map<String, Object>) CommonDatasourceRoute.getInstance().doDataSourceRoute(clientSchema, params);
		demoService.doDelete(params);
	}
	/**
	 * 测试同库事务是否成功
	 * @throws Exception
	 */
	public void testInsertWithRollBack() throws Exception{
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		CommonDatasourceSchema clientSchema=(CommonDatasourceSchema) context.getBean("democlient");
		IDemoService demoService=(IDemoService) context.getBean("demoService");
		DemoParam demoParam=new DemoParam();
		demoParam.setId("2");
		demoParam.setName("liubing流1");
		demoParam.setNumber("14");
		demoParam= (DemoParam) CommonDatasourceRoute.getInstance().doDataSourceRoute(clientSchema, demoParam);
		demoService.doCreate(demoParam,true);
	}
	/**
	 * 
	 * @throws Exception
	 */
	public void testInsertWithNORollBack() throws Exception{
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		CommonDatasourceSchema clientSchema=(CommonDatasourceSchema) context.getBean("democlient");
		IDemoService demoService=(IDemoService) context.getBean("demoService");
		DemoParam demoParam=new DemoParam();
		demoParam.setId("3");
		demoParam.setName("liubing流1");
		demoParam.setNumber("14");
		demoParam= (DemoParam) CommonDatasourceRoute.getInstance().doDataSourceRoute(clientSchema, demoParam);
		demoService.doCreate(demoParam,false);
	}
	
	public void testUpdate() throws Exception{
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		CommonDatasourceSchema clientSchema=(CommonDatasourceSchema) context.getBean("democlient");
		IDemoService demoService=(IDemoService) context.getBean("demoService");
		DemoParam demoParam=new DemoParam();
		demoParam.setId("3");
		demoParam.setName("liubing流2");
		demoParam.setNumber("14");
		demoParam= (DemoParam) CommonDatasourceRoute.getInstance().doDataSourceRoute(clientSchema, demoParam);
		demoService.doUpdate(demoParam);
	}
	
	/**
	 * 扩展事务拦截器，根据参数选择数据源
	 * service 实现类第一个参数必须是hashmap 或者javabean .做分库分表的依据
	 * @throws Exception
	 */
	public void testFindWithInterceptor()throws Exception{
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("springInterceptor.xml");
		IDemoService demoService=(IDemoService) context.getBean("demoService");
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("id", 3);
		DemoModel demoModel=demoService.findById(params);
		System.out.println("name:"+demoModel.getName()+",id:"+demoModel.getId()+",number:"+demoModel.getNumber());
	}
	/**
	 * 读写分离
	 * @throws Exception
	 */
	public void testFindWithReadAndWrite()throws Exception{
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("springReadAndWrite.xml");
		IDemoService demoService=(IDemoService) context.getBean("demoService");
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("id", 3);
		DemoModel demoModel=demoService.findById(params);
		System.out.println("name:"+demoModel.getName()+",id:"+demoModel.getId()+",number:"+demoModel.getNumber());
	}
}
