/**
 * 
 */
package test.common.distributed.datasource.ibatis.dao.impl;

import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import test.common.distributed.datasource.ibatis.dao.IDemoDao;
import test.common.distributed.datasource.ibatis.model.DemoModel;
import test.common.distributed.datasource.ibatis.model.DemoParam;


/**
 * @author liubing1
 *
 */
public class DemoDaoImpl extends SqlMapClientDaoSupport implements IDemoDao {

	
	@Override
	public void insert(DemoParam demoParam) throws Exception{
		// TODO Auto-generated method stub
		getSqlMapClientTemplate().insert("demo.insert",demoParam);
	}

	
	@Override
	public void delete(Map<String, Object> params) throws Exception{
		// TODO Auto-generated method stub
		getSqlMapClientTemplate().delete("demo.delete", params);
	}

	
	@Override
	public void update(DemoParam demoParam) throws Exception{
		// TODO Auto-generated method stub
		getSqlMapClientTemplate().update("demo.update", demoParam);
	}

	
	@Override
	public DemoModel findById(Map<String, Object> params) throws Exception{
		// TODO Auto-generated method stub
		return (DemoModel) getSqlMapClientTemplate().queryForObject("demo.findById", params);
	}

}
