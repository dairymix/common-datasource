/**
 * 
 */
package test.common.distributed.datasource.ibatis.model;

import java.io.Serializable;

/**
 * @author liubing1
 *
 */
public class DemoParam implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8363232267419084842L;

	private String id;

	private String name;

	private String number;
	
	private String tablename;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the tablename
	 */
	public String getTablename() {
		return tablename;
	}

	/**
	 * @param tablename the tablename to set
	 */
	public void setTablename(String tablename) {
		this.tablename = tablename;
	}

	public DemoParam() {
		super();
	}
	
	
}
