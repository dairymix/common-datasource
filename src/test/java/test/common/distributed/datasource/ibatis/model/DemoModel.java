/**
 * 
 */
package test.common.distributed.datasource.ibatis.model;

import java.io.Serializable;

/**
 * @author liubing1
 * 
 */
public class DemoModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8019999759937707364L;

	private String id;

	private String name;

	private String number;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number
	 *            the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * 
	 */
	public DemoModel() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DemoModel [name=" + name + ", number=" + number + "]";
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
}
